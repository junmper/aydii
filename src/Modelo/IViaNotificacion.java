/*
 * interfaz para unificar el envio de notificacion por diferentes medios
 */
package Modelo;

/**
 *
 * @author alejandro
 */
public interface IViaNotificacion {
    public void enviarNotificacion(Notificado notificado, String mensaje);
}
