/*
 * Estrategia para enviar las noficaciones por correo usando SMTP
 */
package Modelo;

/**
 *
 * @author alejandro
 */
public class EstrategiaNotificacion3 implements EstrategiaNotificacion{

    @Override
    public void notificar(Notificado notificado, String mensaje) {
        
        IViaNotificacion notificacion = new ViaNotificacionSMTP();
        
        notificacion.enviarNotificacion(notificado, mensaje);
        
    }
    
}
