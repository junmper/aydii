/*
 * Clase que contiene la informacion del notificado, ya sea una persona en 
 * especifica o una entidad
 */
package Modelo;

/**
 *
 * @author alejandro
 */
public class Notificado {
    private int celular;
    private String correo;
    private String nombre;

    public Notificado(int celular, String correo, String nombre) {
        this.celular = celular;
        this.correo = correo;
        this.nombre = nombre;
    }

    public Notificado() {
    }

    public int getCelular() {
        return celular;
    }

    public void setCelular(int celular) {
        this.celular = celular;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
}
