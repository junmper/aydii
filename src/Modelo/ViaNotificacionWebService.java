/*
 * Clase para la notificaion de envio de la noficacion usando en WebService de la universidad
 */
package Modelo;

/**
 *
 * @author alejandro
 */
public class ViaNotificacionWebService implements IViaNotificacion{

    @Override
    public void enviarNotificacion(Notificado notificado, String mensaje) {
        String correo = notificado.getCorreo();
        
        //ya con el correo se consume el servicio web enviandole como parametros
        //por POST el correo y el mensaje
    }
    
}
