/*
 * Clase para que envia las notificaciones presupuestales
 */
package Modelo;

/**
 *
 * @author alejandro
 */
public class EstrategiaNotificacion1 implements EstrategiaNotificacion{

    @Override
    public void notificar(Notificado notificado, String mensaje) {
         IViaNotificacion notificacion = new ViaNotificacionSMS();
         notificacion.enviarNotificacion(notificado, mensaje);
    }
    
}
