/*
 * Clase que ejecuta la estrategia 
 */
package Modelo;

/**
 *
 * @author alejandro
 */
public class Notifiacion {
    
    private EstrategiaNotificacion estrategia;
    

    public Notifiacion(EstrategiaNotificacion estrategia) {
        this.estrategia = estrategia;
    }
    
    public void ejecutarEstrategia(Notificado notificado, String Mensaje)
    {
        estrategia.notificar(notificado, Mensaje);
    }
    
}
