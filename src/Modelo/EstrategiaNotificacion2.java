/*
 * Estrategia que envia las notificaion por Web Service
 */
package Modelo;

/**
 *
 * @author alejandro
 */
public class EstrategiaNotificacion2 implements EstrategiaNotificacion{

    @Override
    public void notificar(Notificado notificado, String mensaje) {
           IViaNotificacion notificacion = new ViaNotificacionWebService();
           notificacion.enviarNotificacion(notificado, mensaje);
    }
    
}
